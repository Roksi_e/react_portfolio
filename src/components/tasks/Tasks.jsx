/* eslint-disable array-callback-return */

import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'
import { IoArrowBackOutline } from 'react-icons/io5'
import { v4 as uuidv4 } from 'uuid'
import { useTranslation } from 'react-i18next'
import { Button } from '../button/Button'
import { projects } from '../../data'

import styles from './Tasks.module.css'

const pageAnimation = {
	hidden: {
		y: 300,
		opasity: 0
	},
	visible: {
		y: 0,
		opasity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4
		}
	}
}

const Tasks = () => {
	const { t } = useTranslation()
	const [tasks, setTasks] = useState([])

	useEffect(() => {
		projects.map(a => {
			if (a.tasks) {
				setTasks(a.tasks)
			}
		})
	}, [])

	return (
		<motion.main
			initial={'hidden'}
			whileInView={'visible'}
			viewport={{ once: true, amount: 0.4 }}
			exit={'visible'}
			className={styles.main}
		>
			<section>
				<div className={styles.wrapper}>
					<Button text={'Back'} link={'/projects'}>
						<IoArrowBackOutline size={'16px'} />
					</Button>
					<div className={styles.projects}>
						{tasks &&
							tasks.map(project => (
								<motion.div
									initial={'hidden'}
									whileInView={'visible'}
									viewport={{ once: true, amount: 0.4 }}
									key={uuidv4()}
									className={styles.projectBox}
								>
									<motion.div variants={pageAnimation} className={styles.projectCard}>
										<Link to={project.demo} target="_blank" className={styles.imageLink}>
											<div
												className={styles.imageBox}
												style={{
													backgroundImage: `url(${project.image})`
												}}
											></div>
										</Link>

										<div className={styles.projectDescrip}>
											<h3>{project.name}</h3>
											<div className={styles.description}>{t(`projects.${project.description}`)}</div>
											<div className={styles.projectStacks}>
												{project.stacks.map(stack => {
													return (
														<strong className={styles.stackItem} key={uuidv4()}>
															{stack}
														</strong>
													)
												})}
											</div>
											<div className={styles.projectBtn}>
												<Button text={t('buttons.project')} link={project.demo} target="_blank" />
												<Button text={t('buttons.git')} link={project.git} target="_blank" />
											</div>
										</div>
									</motion.div>
								</motion.div>
							))}
					</div>
				</div>
			</section>
		</motion.main>
	)
}

export default Tasks
