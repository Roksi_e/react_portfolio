import { forwardRef } from 'react'
import { motion } from 'framer-motion'
import { useTranslation } from 'react-i18next'

import styles from './contacts.module.css'

export const Contacts = forwardRef(({ children }, ref) => {
	const { t } = useTranslation()
	return (
		<div ref={ref} className={styles.box}>
			<h2>{t('title.contacts')}</h2>
			<div className={styles.contactsBox}>{children}</div>
		</div>
	)
})

export const MContacts = motion.create(Contacts)
