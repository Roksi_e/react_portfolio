import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import logo from '../../img/logo7.png'

import styles from './footer.module.css'

const Footer = () => {
	const { t } = useTranslation()

	return (
		<div className={styles.footerWrapper}>
			<div className={styles.footer}>
				<div className={styles.headerLogo}>
					<img src={logo} alt="logo" />
				</div>
				<div className={styles.headerNav}>
					<ul className={styles.navigation}>
						<li className={styles.navItem}>
							<Link className={styles.link} to=".">
								{t('header.home')}
							</Link>
						</li>
						<li className={styles.navItem}>
							<Link to="about" className={styles.link}>
								{t('header.aboutMe')}
							</Link>
						</li>
						<li className={styles.navItem}>
							<Link to="projects" className={styles.link}>
								{t('header.projects')}
							</Link>
						</li>
					</ul>
				</div>
				<h3>{`${new Date().getFullYear()} - Inna Tymoshenko`}</h3>
			</div>
		</div>
	)
}

export default Footer
