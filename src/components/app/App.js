import { Route, Routes } from 'react-router-dom'
import PageOne from '../../pages/page-one/Pageone'
import PageContacts from '../../pages/page-contacts/PageContacts'
import PageProjects from '../../pages/page-projects/PageProjects'
import PageAbout from '../../pages/page-about/PageAbout'
import NotFound from '../../pages/not-found/NotFound'
import MainLayouts from '../../methods/MainLayouts'
import Footer from '../footer/Footer'

import styles from './app.module.css'
import Tasks from '../tasks/Tasks'

const App = () => {
	return (
		<>
			<div className={styles.wrapper}>
				<Routes>
					<Route path="/" element={<MainLayouts />}>
						<Route index element={<PageOne />} />
						<Route path="about" element={<PageAbout />} />
						<Route path="projects" element={<PageProjects />} />
						<Route path="projects/tasks" element={<Tasks />} />
						<Route path="contacts" element={<PageContacts />} />
						<Route path="*" element={<NotFound />} />
					</Route>
				</Routes>
				<footer>
					<Footer />
				</footer>
			</div>
		</>
	)
}

export default App
