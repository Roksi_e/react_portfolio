import { LuMoon, LuSun } from 'react-icons/lu'
import { AiOutlineMenu, AiOutlineClose } from 'react-icons/ai'
import { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import useTheme from '../../methods/useTheme'
import logo from '../../img/logo7.png'
import { LOCALS } from '../translation/constans'

import styles from './navigation.module.css'

const Navigation = () => {
	const { t, i18n } = useTranslation()
	const [scroll, setScroll] = useState({ y: 0, lastY: 0 })
	const [show, setShow] = useState(true)
	const [icon, setIcon] = useState()
	const [closeHead, setCloseHead] = useState(false)

	const { theme, setTheme } = useTheme()

	const scrollNav = () => {
		setScroll(prevState => {
			return {
				y: window.scrollY,
				lastY: prevState.y
			}
		})
	}

	const closeMenu = () => {
		setCloseHead(true)
	}

	const activeIcon = () => {
		if (theme === 'dark') {
			setTheme('light')
			setIcon(false)
		} else {
			setTheme('dark')
			setIcon(true)
		}
	}

	useEffect(() => {
		window.addEventListener('scroll', scrollNav)
		return () => {
			window.removeEventListener('scroll', scrollNav)
		}
	}, [])

	useEffect(() => {
		if (scroll.y > 500) {
			setShow(true)
		} else {
			setShow(false)
		}
		if (scroll.lastY < scroll.y) {
			setShow(true)
		} else {
			setShow(false)
		}
	}, [scroll])

	return (
		<header>
			<div className={!closeHead ? (show ? styles.headerHide : styles.header) : styles.header}>
				<div className={styles.headerLogo}>
					<img src={logo} alt="logo" />
				</div>
				<div
					className={styles.headerPop}
					style={!closeHead ? { left: '-100%' } : { left: '0px' }}
					onClick={() => setCloseHead(false)}
				>
					<div className={closeHead ? [styles.headerNav, styles.activeNav].join(' ') : [styles.headerNav]}>
						<ul className={styles.navigation}>
							<li className={styles.navItem}>
								<NavLink
									className={({ isActive }) => (isActive ? styles.active : styles.link)}
									to="."
									onClick={() => setCloseHead(false)}
								>
									{t('header.home')}
								</NavLink>
							</li>
							<li className={styles.navItem}>
								<NavLink
									to="about"
									className={({ isActive }) => (isActive ? styles.active : styles.link)}
									onClick={() => setCloseHead(false)}
								>
									{t('header.aboutMe')}
								</NavLink>
							</li>
							<li className={styles.navItem}>
								<NavLink
									to="projects"
									className={({ isActive }) => (isActive ? styles.active : styles.link)}
									onClick={() => setCloseHead(false)}
								>
									{t('header.projects')}
								</NavLink>
							</li>
							<li className={styles.navItem}>
								<NavLink
									to="contacts"
									className={({ isActive }) => (isActive ? styles.active : styles.link)}
									onClick={() => setCloseHead(false)}
								>
									{t('header.contacts')}
								</NavLink>
							</li>
						</ul>
					</div>
				</div>
				<div className={styles.buttons}>
					<button
						className={styles.iconBtn}
						onClick={e => {
							activeIcon()
						}}
					>
						{icon ? <LuSun className={styles.iconActive} /> : <LuMoon className={styles.icon} />}
					</button>
					<button
						disabled={i18n.language === LOCALS.EN}
						className={styles.lanBtn}
						onClick={() => i18n.changeLanguage(LOCALS.EN)}
					>
						EN
					</button>
					<button
						disabled={i18n.language === LOCALS.UK}
						className={styles.lanBtn}
						onClick={() => i18n.changeLanguage(LOCALS.UK)}
					>
						UK
					</button>
				</div>
				<div
					className={styles.mobileBtn}
					onClick={() => {
						setCloseHead(!closeHead)
					}}
				>
					{!closeHead ? (
						<AiOutlineMenu
							className={styles.closePop}
							onClick={() => {
								closeMenu()
							}}
						/>
					) : (
						<AiOutlineClose
							className={styles.closePop}
							onClick={() => {
								closeMenu()
							}}
						/>
					)}
				</div>
			</div>
		</header>
	)
}

export default Navigation
