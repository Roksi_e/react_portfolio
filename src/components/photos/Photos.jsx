import { motion } from 'framer-motion'
import { forwardRef } from 'react'

export const Photos = forwardRef(({ className, photo }, ref) => {
	return (
		<div ref={ref} className={className}>
			<img src={photo} alt="myPhoto" />
		</div>
	)
})

export const MPhotos = motion.create(Photos)
