import { forwardRef } from 'react'
import { motion } from 'framer-motion'
import { v4 as uuidv4 } from 'uuid'
import { useTranslation } from 'react-i18next'
import { skills } from '../../data'

import styles from './mySkills.module.css'

const pageAnimation = {
	hidden: {
		y: 300,
		opacity: 0
	},
	visible: custom => ({
		y: 0,
		opacity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4,
			delay: custom * 0.2
		}
	})
}

const iconAnimation = {
	hidden: {
		x: -300,
		opacity: 0
	},
	visible: custom => ({
		x: 0,
		opacity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4,
			delay: custom * 0.2
		}
	})
}

export const MySkills = forwardRef((props, ref) => {
	const { t } = useTranslation()

	return (
		<motion.div
			ref={ref}
			initial={'hidden'}
			whileInView={'visible'}
			viewport={{ once: true, amount: 0.2 }}
			className={styles.box}
		>
			<motion.h2 variants={pageAnimation}>{t('title.skills')}</motion.h2>
			<motion.div
				initial={'hidden'}
				whileInView={'visible'}
				viewport={{ once: true, amount: 0.2 }}
				className={styles.skillsBox}
			>
				{skills.map((skill, i) => {
					return <MSkillsItem variants={iconAnimation} custom={i + 1} key={uuidv4()} {...skill} />
				})}
			</motion.div>
		</motion.div>
	)
})

const SkillsItem = forwardRef(({ icon, name }, ref) => {
	return (
		<div ref={ref} className={styles.card}>
			<div className={styles.cardImg}>
				<img src={icon} alt={name} />
			</div>
			<p>{name}</p>
		</div>
	)
})

const MSkillsItem = motion.create(SkillsItem)

export const MMySkills = motion.create(MySkills)
