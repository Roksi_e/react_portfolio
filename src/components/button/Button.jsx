import { forwardRef } from 'react'
import { motion } from 'framer-motion'
import styles from './button.module.css'

export const Button = forwardRef(({ text, link, download, target, children }, ref) => {
	return (
		<button ref={ref} className={styles.button}>
			<a href={link} target={target} download={download} rel="noreferrer">
				{children}
				{text}
			</a>
		</button>
	)
})

export const MButton = motion.create(Button)
