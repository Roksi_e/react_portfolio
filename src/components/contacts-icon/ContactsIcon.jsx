import { forwardRef } from 'react'
import { motion } from 'framer-motion'

import styles from './contactsIcon.module.css'

export const ContactIcon = forwardRef(({ link, image, name }, ref) => {
	return (
		<a ref={ref} className={styles.contLink} href={link} target="_blank" rel="noreferrer">
			<img src={image} alt={name} />
		</a>
	)
})

export const MContactIcon = motion.create(ContactIcon)
