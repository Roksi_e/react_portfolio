import { Link } from 'react-router-dom'
import styles from '../Slider.module.css'

const SliderItems = ({ activeIndex, sliderImage }) => {
	return (
		<div>
			{sliderImage.map((slide, index) => {
				// console.log(slide)
				return (
					<Link to={slide.demo} key={index} target="_blank">
						<div className={index === activeIndex ? [styles.slides, styles.active].join(' ') : styles.inactive}>
							<img src={slide.image} alt={slide.name} className={styles.slideImage} />
						</div>
					</Link>
				)
			})}
		</div>
	)
}

export default SliderItems
