import styles from '../Slider.module.css'

const Dots = ({ activeIndex, onclick, sliderImage }) => {
	return (
		<div className={styles.allDots}>
			{sliderImage.map((_, index) => (
				<span
					key={index}
					className={`${activeIndex === index ? [styles.dot, styles.activeDot].join(' ') : styles.dot}`}
					onClick={() => onclick(index)}
				></span>
			))}
		</div>
	)
}

export default Dots
