import { useState, useEffect, forwardRef } from 'react'
import { motion } from 'framer-motion'
import SliderItems from './sliderItems/SliderItems'
import Dots from './sliderItems/Dots'
import Arrows from './sliderItems/Arrow'
import { projects } from '../../data'

import styles from './Slider.module.css'

const len = projects.length - 1

export const Slider = forwardRef((props, ref) => {
	const [activeIndex, setActiveIndex] = useState(0)

	useEffect(() => {
		const interval = setInterval(() => {
			setActiveIndex(activeIndex === len ? 0 : activeIndex + 1)
		}, 3000)
		return () => clearInterval(interval)
	}, [activeIndex])

	return (
		<div ref={ref} className={styles.sliderContainer}>
			<SliderItems activeIndex={activeIndex} sliderImage={projects} />
			<Arrows
				prevSlide={() => setActiveIndex(activeIndex < 1 ? len : activeIndex - 1)}
				nextSlide={() => setActiveIndex(activeIndex === len ? 0 : activeIndex + 1)}
			/>
			<Dots activeIndex={activeIndex} sliderImage={projects} onclick={activeIndex => setActiveIndex(activeIndex)} />
		</div>
	)
})

export const MSlider = motion.create(Slider)
