import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import en from './catalog/en/en'
import uk from './catalog/uk/uk'
import { LOCALS } from './constans'

const resources = {
	[LOCALS.EN]: {
		translation: en
	},
	[LOCALS.UK]: {
		translation: uk
	}
}

i18n
	.use(initReactI18next)
	.use(LanguageDetector)
	.init({
		resources,
		fallbackLng: 'en',
		interpolation: {
			escapeValue: false
		}
	})

export default i18n
