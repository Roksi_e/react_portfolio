import html from '../img/icon/icon_html.png'
import react from '../img/icon/React-icon.svg.png'
import bootstrap from '../img/icon/bootstrap-logo-shadow.png'
import css from '../img/icon/icon_css.png'
import git from '../img/icon/icon_git.png'
import js from '../img/icon/icon_js.png'
import node from '../img/icon/node icon.png'
import redux from '../img/icon/redux-icon.png'
import typescript from '../img/icon/typescript-icon.png'
import next from '../img/icon/nextjs-icon.png'
// import docker from '../img/icon/docker_icon.png'
import tailwind from '../img/icon/tailwind.png'
import vite from '../img/icon/Vite.png'
import figma from '../img/icon/figma.png'
import linkedIn from '../img/contacts/linkedIn.png'
import facebook from '../img/contacts/facebook.png'
import telegram from '../img/contacts/teleg.png'
import email from '../img/contacts/mail2.png'
import cinema from '../img/projects/cinema1.png'
import rick from '../img/projects/rick2.png'
import library from '../img/projects/library.png'
import dapper from '../img/projects/Dapper_Bully1.png'
import sportif from '../img/projects/Sportif1.png'
import miss from '../img/projects/Miss_cupcake1.png'
import easybank from '../img/projects/esybank1.png'
import comments from '../img/projects/int-c2.png'
import manage from '../img/projects/manage.png'
import todo from '../img/projects/todo5.png'
import countries from '../img/projects/rest3.png'
import shortening from '../img/projects/url.png'
import ecommerce from '../img/projects/ecommerce.png'
import dessert from '../img/projects/dessert.png'
import multyStep from '../img/projects/multy-form.png'
import space from '../img/projects/space.png'
import home from '../img/projects/home-page.png'
import job_listing from '../img/projects/job-listing3.png'

export const skills = [
	{
		name: 'HTML',
		icon: html
	},
	{
		name: 'CSS',
		icon: css
	},
	{
		name: 'JavaScript',
		icon: js
	},
	{
		name: 'Bootstrap',
		icon: bootstrap
	},
	{
		name: 'React',
		icon: react
	},
	{
		name: 'Git',
		icon: git
	},

	{
		name: 'Redux',
		icon: redux
	},
	{
		name: 'TypeScript',
		icon: typescript
	},
	{
		name: 'Next.js',
		icon: next
	},
	{
		name: 'TailwindCSS',
		icon: tailwind
	},
	{
		name: 'Vite',
		icon: vite
	},
	{
		name: 'Figma',
		icon: figma
	},
	{
		name: 'React Native',
		icon: react
	},
	{
		name: 'Node JS',
		icon: node
	}
	// {
	// 	name: 'Docker',
	// 	icon: docker
	// },
]

export const contacts = [
	{
		name: 'telegram',
		link: 'https://t.me/Roksi_e',
		image: telegram
	},
	{
		name: 'linkedIn',
		link: 'https://www.linkedin.com/in/innatymoshenko/',
		image: linkedIn
	},
	{
		name: 'facebook',
		link: 'https://www.facebook.com/inna.tymoshenko.9',
		image: facebook
	},
	{
		name: 'email',
		link: 'mailto:innatymosh@dmail.com',
		image: email
	}
]

export const projects = [
	{
		name: 'Frontend Mentor',
		description: 'frontend_mentor',
		image: comments,
		stacks: [],
		demo: 'projects/tasks',
		git: '',
		tasks: [
			{
				name: 'Job listings',
				description: 'job-listing',
				image: job_listing,
				stacks: ['React', 'Next.js', 'TypeScript', 'tailwind'],
				demo: 'https://static-job-listings-alpha-wine.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/static-job-listings'
			},
			{
				name: 'Room homepage',
				description: 'room-homepage',
				image: home,
				stacks: ['React', 'Next.js', 'TypeScript', 'tailwind'],
				demo: 'https://room-homepage-wine.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/room-homepage'
			},
			{
				name: 'Space tourism multi-page website',
				description: 'space',
				image: space,
				stacks: ['React', 'Next.js', 'TypeScript', 'tailwind', 'Framer motion'],
				demo: 'https://space-tourism-virid-chi.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/space-tourism'
			},
			{
				name: 'Multy step form',
				description: 'multy_step_form',
				image: multyStep,
				stacks: ['React', 'Next.js', 'TypeScript', 'tailwind', 'React-hook-form', 'zod', 'zustand'],
				demo: 'https://multi-step-form-beta-self.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/multi-step-form'
			},
			{
				name: 'Product list',
				description: 'product_list',
				image: dessert,
				stacks: ['React', 'Next.js', 'TypeScript', 'tailwind', 'zustand'],
				demo: 'https://product-list-zeta-two.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/product-list'
			},
			{
				name: 'E-commerce product page',
				description: 'ecommerce',
				image: ecommerce,
				stacks: ['React', 'TypeScript', 'tailwind', 'zustand', 'vite'],
				demo: 'https://ecommerce-product-page-ten-lac.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/ecommerce-product-page'
			},
			{
				name: 'Interactive comments section',
				description: 'comments_section',
				image: comments,
				stacks: ['React', 'TypeScript', 'tailwind', 'zustand', 'axios', 'vite'],
				demo: 'https://interactive-comments-rho.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/interactive-comments'
			},
			{
				name: 'URL shortening API',
				description: 'shortening',
				image: shortening,
				stacks: ['React', 'Redux/Toolkit', 'React-hook-form'],
				demo: 'https://url-shortening-api-roan.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/url-shortening-api'
			},
			{
				name: 'REST Countries API',
				description: 'rest_countries',
				image: countries,
				stacks: ['React', 'styled-components', 'axios'],
				demo: 'https://rest-countries-api-amber-ten.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/rest_countries-api'
			},
			{
				name: 'Manage-landing-page',
				description: 'manage_landing',
				image: manage,
				stacks: ['React', 'Next.js', 'TypeScript', 'tailwind', 'zod'],
				demo: 'https://manage-landing-page-delta-five.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/manage-landing-page'
			},
			{
				name: 'Todo-app',
				description: 'todo_app',
				image: todo,
				stacks: ['React', 'TypeScript', 'zustand'],
				demo: 'https://todo-app-six-theta-28.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/todo-app'
			},
			{
				name: 'Easybank landing page',
				description: 'easybank',
				image: easybank,
				stacks: ['React', 'TypeScript'],
				demo: 'https://esybank.vercel.app/',
				git: 'https://github.com/InnaTymoshenko/esybank'
			}
		]
	},
	{
		name: 'Library Books',
		description: 'library',
		image: library,
		stacks: ['JavaScript', 'React', 'Redux/Toolkit', 'axios', 'React Bootstrap'],
		demo: 'https://library-books-sage.vercel.app/',
		git: 'https://gitlab.com/Roksi_e/library-books.git'
	},
	{
		name: 'Cinema',
		description: 'cinema',
		image: cinema,
		stacks: ['JavaScript', 'React', 'React-hook-form', 'React Bootstrap'],
		demo: 'https://cinema-ashen-eta.vercel.app/',
		git: 'https://gitlab.com/Roksi_e/cinema.git'
	},
	{
		name: 'Rick&Morty',
		description: 'rick',
		image: rick,
		stacks: ['JavaScript', 'React', 'Next.js', 'TypeScript', 'axios', 'React-Query', 'zustand', 'tailwind'],
		demo: 'https://rick-morty-next-theta.vercel.app/',
		git: 'https://gitlab.com/Roksi_e/rick_morty.next.git'
	},
	{
		name: 'Miss Cupcake',
		description: 'miss',
		image: miss,
		stacks: ['JavaScript', 'HTML', 'CSS', 'Bootstrap'],
		demo: 'https://miss-cupcake-three.vercel.app/',
		git: 'https://gitlab.com/Roksi_e/miss_cupcake.git'
	},
	{
		name: 'Sportif',
		description: 'sportif',
		image: sportif,
		stacks: ['JavaScript', 'HTML', 'CSS'],
		demo: 'https://sportif-sigma.vercel.app/',
		git: 'https://gitlab.com/Roksi_e/sportif.git'
	},
	{
		name: 'Dapper Bully',
		description: 'dapper',
		image: dapper,
		stacks: ['HTML', 'CSS'],
		demo: 'https://dapper-bully.vercel.app/',
		git: 'https://gitlab.com/Roksi_e/dapper_bully.git'
	}
]
