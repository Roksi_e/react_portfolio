import { useTranslation } from 'react-i18next'
import { motion } from 'framer-motion'
import { v4 as uuidv4 } from 'uuid'
import { Button, MButton } from '../../components/button/Button'
import { MContacts } from '../../components/contacts/Contacts'
import { Photos } from '../../components/photos/Photos'
import { MContactIcon } from '../../components/contacts-icon/ContactsIcon'
import { MSlider } from '../../components/slider/Slider'
import photo from '../../img/photo_one.png'
import { contacts } from '../../data'
import '../../components/translation/i18next'

import styles from './pageone.module.css'

const pageAnimation = {
	hidden: {
		y: 300,
		opasity: 0
	},
	visible: custom => ({
		y: 0,
		opasity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4,
			delay: custom * 0.2
		}
	})
}

const iconAnimation = {
	hidden: {
		x: -300,
		opacity: 0
	},
	visible: custom => ({
		x: 0,
		opacity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4,
			delay: custom * 0.2
		}
	})
}

const PageOne = () => {
	const { t } = useTranslation()

	return (
		<motion.main
			initial={'hidden'}
			whileInView={'visible'}
			viewport={{ once: true, amount: 0.2 }}
			className={styles.main}
		>
			<section>
				<div className={styles.about}>
					<Photos className={styles.aboutImage} photo={photo} />
					<div className={styles.aboutContext}>
						<h2 className={styles.aboutTitle}>{t('pageOne.about')}</h2>
						{/* <div>OIEHJFHJB</div> */}
						<div>{t('pageOne.hello')}</div>
						<div className={styles.aboutBtn}>
							<Button text={t('buttons.seeMore')} link="about" target="_self" />
							<Button text="LinkedIn" link="https://www.linkedin.com/in/innatymoshenko/" target="_blank" />
						</div>
					</div>
				</div>
			</section>
			<motion.section initial={'hidden'} whileInView={'visible'} viewport={{ once: true, amount: 0.2 }}>
				<div className={styles.projects}>
					<motion.h2 custom={1} variants={pageAnimation}>
						{t('title.projects')}
					</motion.h2>
					<MSlider custom={2} variants={pageAnimation} />
					<div>
						<MButton custom={3} variants={pageAnimation} text={t('buttons.seeMore')} link="projects" target="_self" />
					</div>
				</div>
			</motion.section>
			<motion.section initial={'hidden'} whileInView={'visible'} viewport={{ once: true, amount: 0.2 }}>
				<MContacts variants={pageAnimation}>
					<motion.div
						initial={'hidden'}
						whileInView={'visible'}
						viewport={{ once: true, amount: 0.2 }}
						className={styles.contIcon}
					>
						{contacts.map((el, i) => {
							return <MContactIcon custom={i + 1} variants={iconAnimation} key={uuidv4()} {...el} />
						})}
					</motion.div>
					{/* <a
						href="https://freelancehunt.com/freelancer/inna_tymoshenko.html?from=shield&r=nw9Xe"
						rel="noreferrer"
						target="_blank"
					>
						<img
							src="https://freelancehunt.com/shields/display/id/1598642/type/rating?style=social&amp;lang=uk&amp;showLogin=1"
							alt="Freelancehunt — простий та чесний фриланс"
						/>
					</a> */}
				</MContacts>
			</motion.section>
		</motion.main>
	)
}

export default PageOne
