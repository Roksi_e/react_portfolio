import { Contacts } from '../../components/contacts/Contacts'
import { v4 as uuidv4 } from 'uuid'
import { ContactIcon } from '../../components/contacts-icon/ContactsIcon'
import { contacts } from '../../data'

import styles from './page-contacts.module.css'
import { Button } from '../../components/button/Button'

const PageContacts = () => {
	return (
		<main className={styles.main}>
			<section>
				<Contacts>
					<div className={styles.contacts}>
						<div className={styles.item}>
							<b>Email:</b> innatymosh@gmail.com
						</div>
						<div className={styles.item}>
							<b>Phone:</b> +38(050) 908 49 96
						</div>
						<div></div>
					</div>
					<div className={styles.contIcon}>
						{contacts.map((el, i) => {
							return <ContactIcon key={uuidv4()} {...el} />
						})}
					</div>
					<Button text={'Look Git'} link={'https://gitlab.com/Roksi_e/react_portfolio'} target="_blank" />
				</Contacts>
			</section>
		</main>
	)
}

export default PageContacts
