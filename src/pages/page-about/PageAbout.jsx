import { useTranslation } from 'react-i18next'
import { motion } from 'framer-motion'
import { Button } from '../../components/button/Button'
import { Photos, MPhotos } from '../../components/photos/Photos'
import { MMySkills } from '../../components/mySkills/MySkills'
import photo2 from '../../img/photo2.png'
import CV_photo from '../../img/CV.png'
import CV from '../../file/Inna_Tymoshenko_CV.pdf'

import styles from './pageAbout.module.css'

const pageAnimation = {
	hidden: {
		y: 300,
		opasity: 0
	},
	visible: custom => ({
		y: 0,
		opasity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4,
			delay: custom * 0.2
		}
	})
}

const PageAbout = () => {
	const { t } = useTranslation()

	return (
		<motion.main
			initial={'hidden'}
			whileInView={'visible'}
			viewport={{ once: true, amount: 0.2 }}
			exit={'visible'}
			className={styles.main}
		>
			<section>
				<div className={styles.about}>
					<div className={styles.aboutContext}>
						<h2>{t('pageAbout.message')}</h2>
						<div>
							<p>{t('pageAbout.about.1')}</p>
							<p>{t('pageAbout.about.2')}</p>
							<p>{t('pageAbout.about.3')}</p>
						</div>
					</div>
					<Photos className={styles.aboutImage} photo={photo2} />
				</div>
			</section>
			<section>
				<MMySkills custom={1} variants={pageAnimation} />
			</section>
			<motion.section initial={'hidden'} whileInView={'visible'} viewport={{ once: true, amount: 0.2 }}>
				<div className={styles.boxCV}>
					<motion.h2 custom={2} variants={pageAnimation}>
						{t('title.cv')}
					</motion.h2>
					<MPhotos custom={3} variants={pageAnimation} className={styles.aboutImageCV} photo={CV_photo} />
				</div>
			</motion.section>
			<motion.section initial={'hidden'} whileInView={'visible'} viewport={{ once: true, amount: 0.2 }}>
				<motion.div custom={4} variants={pageAnimation} className={styles.aboutBtn}>
					<Button text={t('buttons.download')} link={CV} download="" />
					<Button text="LinkedIn" link="https://www.linkedin.com/in/innatymoshenko/" target="_blank" />
				</motion.div>
			</motion.section>
		</motion.main>
	)
}

export default PageAbout
