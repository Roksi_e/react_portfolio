const NotFound = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <h2>Not Found</h2>
    </div>
  );
};

export default NotFound;
