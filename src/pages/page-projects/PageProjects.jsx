import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'
import { v4 as uuidv4 } from 'uuid'
import { useTranslation } from 'react-i18next'
import { Button } from '../../components/button/Button'
import { projects } from '../../data'
import { randomInt, shuffle } from '../../methods/fn'

import styles from './page-projects.module.css'

const pageAnimation = {
	hidden: {
		y: 300,
		opacity: 0
	},
	visible: {
		y: 0,
		opacity: 1,
		transition: {
			duration: 0.8,
			type: 'spring',
			bounce: 0.4
		}
	}
}

const taskImage = {
	hidden: {
		y: -300,
		opacity: 0
	},
	visible: custom => ({
		y: 0,
		opacity: 1,
		transition: {
			duration: 0.5,
			delay: custom * 0.2
		}
	})
}

const tasksAnimate = {
	hidden: {
		boxShadow: 'none',
		opacity: 0
	},
	visible: custom => ({
		boxShadow: '3px 3px 5px 0 rgb(87, 86, 86)',
		opacity: 1,
		transition: {
			duration: 0.5,
			delay: custom * 0.2
		}
	})
}

const PageProjects = () => {
	const { t } = useTranslation()
	const [uniqueStacks, setUniqueStacks] = useState([])

	useEffect(() => {
		// eslint-disable-next-line array-callback-return
		projects.map(project => {
			if (project.tasks) {
				const uniqueStack = [...new Set(project.tasks.flatMap(task => task.stacks))]

				setUniqueStacks(uniqueStack)
			}
		})
	}, [])

	return (
		<motion.main
			initial={'hidden'}
			whileInView={'visible'}
			viewport={{ once: true, amount: 0.4 }}
			exit={'visible'}
			className={styles.main}
		>
			<section>
				<div className={styles.projects}>
					<h2>{t('title.projects')}</h2>
					{projects.map((project, i) => {
						if (project.tasks) {
							shuffle(project.tasks)
						}
						return (
							<motion.div
								initial={'hidden'}
								whileInView={'visible'}
								viewport={{ once: true, amount: 0.4 }}
								key={uuidv4()}
								className={styles.projectBox}
							>
								<motion.div variants={pageAnimation} className={styles.projectCard}>
									<Link
										to={project.tasks ? 'tasks' : project.demo}
										target={project.tasks ? '_self' : '_blank'}
										className={styles.imageLink}
									>
										<div
											id="images"
											className={styles.imageBox}
											style={
												project.tasks
													? { position: 'relative' }
													: {
															backgroundImage: `url(${project.image})`
													  }
											}
										>
											{project.tasks &&
												project.tasks.map((t, i) => (
													<motion.div
														variants={tasksAnimate}
														custom={i + 1}
														key={i}
														className={styles.tasksImg}
														style={{
															zIndex: i + 1,
															transform: `rotate(${randomInt(-10, 10)}deg) translate(${randomInt(
																-40,
																40
															)}px, ${randomInt(-40, 40)}px)`
														}}
													>
														<motion.img variants={taskImage} custom={i + 1} src={t.image} alt={t.name} />
													</motion.div>
												))}
										</div>
									</Link>

									<div className={styles.projectDescrip}>
										<h3>{project.name}</h3>
										<div className={styles.description}>{t(`projects.${project.description}`)}</div>
										<div className={styles.projectStacks}>
											{project.stacks.length !== 0
												? project.stacks.map(stack => (
														<strong className={styles.stackItem} key={uuidv4()}>
															{stack}
														</strong>
												  ))
												: uniqueStacks &&
												  uniqueStacks.map(stack => (
														<strong className={styles.stackItem} key={uuidv4()}>
															{stack}
														</strong>
												  ))}
										</div>
										<div className={styles.projectBtn}>
											<Button
												text={t('buttons.project')}
												link={project.demo}
												target={project.tasks ? '_self' : '_blank'}
											/>
											{project.git && <Button text={t('buttons.git')} link={project.git} target="_blank" />}
										</div>
									</div>
								</motion.div>
							</motion.div>
						)
					})}
				</div>
			</section>
		</motion.main>
	)
}

export default PageProjects
