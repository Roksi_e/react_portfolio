export function randomInt(min, max) {
	let rand = min + Math.random() * (max + 1 - min)
	return Math.floor(rand)
}

export function shuffle(array) {
	array.sort(() => Math.random() - 0.5)
}
