import { Outlet } from "react-router-dom";
import Navigation from "../components/navigation/Navigation";

const MainLayouts = () => {
  return (
    <>
      <Navigation />
      <Outlet />
    </>
  );
};

export default MainLayouts;
